    var baseUrl = 'https://rest.ehrscape.com/rest/v1';
    var queryUrl = baseUrl + '/query';
    var username = "ois.seminar";
    var password = "ois4fri";
    var sessionId;
    function getSessionId() {
        var response = $.ajax({
            type: "POST",
            url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                    "&password=" + encodeURIComponent(password),
            async: false
        });
        return response.responseJSON.sessionId;
    }
    function generirajPodatke() {
    ehrId = "";

    // TODO: Potrebno implementirati

    return ehrId;
    }
function preberiMeritveVitalnihZnakov() {
    sessionId = getSessionId();

    var ehrId = $("#preberiObstojeciEHR").val();

    console.log("Zahtevam podatke za " + ehrId);
    if (!ehrId || ehrId.trim().length == 0) {
        $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Vnešen ehrID ni veljaven");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var party = data.party;
                $("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
                "podatkov za <b>" + "krvni tlak" + "</b> bolnika <b>'" + party.firstNames +
                    " " + party.lastNames + "'</b>.</span><br/><br/>");
                //http://www.openehr.org/releases/QUERY/latest/docs/AQL/AQL.html
                    var AQL =
                        "select \
                        t/data[at0001]/events[at0006]/time/value as cas, \
                        t/data[at0001]/events[at0006]/data[at0003]/items[at0004]/value/magnitude as tlak_s, \
                        t/data[at0001]/events[at0006]/data[at0003]/items[at0005]/value/magnitude as tlak_d \
                        from EHR e[e/ehr_id/value='" + ehrId + "'] \
                        contains OBSERVATION t[openEHR-EHR-OBSERVATION.blood_pressure.v1] \
                        order by t/data[at0001]/events[at0006]/time/value desc \
                        limit 31";
                    $.ajax({
                        url: baseUrl + "/query?" + $.param({"aql": AQL}),
                        type: 'GET',
                        headers: {"Ehr-Session": sessionId},
                        success: function (res) {
                        if (res) {
                                $("#rezultatMeritveVitalnihZnakov").append("Poizvedva uspela");
                                console.log(res);
                        } else {
                                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                    "<span class='obvestilo label label-warning fade-in'>" +
                                    "Ni podatkov!</span>");
                            }
                        },
                        error: function(err) {
                            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                        }
                    });
            },
            error: function(err) {
                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}
function dodajMeritveVitalnihZnakov() {
    sessionId = getSessionId();

    var ehrId = $("#preberiObstojeciEHR").val();
    var datumInUra = $("#dodajDatum").val();
    var s = datumInUra.split("-");
    s[0]=((s[0]-0)+6000);
    datumInUra=s.join("-");
    var telesnaVisina = 170;
    var telesnaTeza = 80;
    var telesnaTemperatura = 37;
    var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
    var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
    var nasicenostKrviSKisikom = 98;
    var merilec = "Papa";
    console.log(datumInUra);
    console.log("PostEHR:", ehrId);

        if (sistolicniKrvniTlak === "" || diastolicniKrvniTlak === "" || datumInUra === "") {
            console.log("Ni podatkov");
        $("#statusZahteve").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        console.log("Podatki so");
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        var podatki = {
            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en",
            "ctx/territory": "SI",
            "ctx/time": datumInUra,
            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
            "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
            "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
            "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        };
        var parametriZahteve = {
            ehrId: ehrId,
            templateId: 'Vital Signs',
            format: 'FLAT',
            committer: merilec
        };
        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            success: function (res) {
                $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" + "Meritev dodana!</span>");
                console.log(res.meta.href)
            },
            error: function(err) {
                $("#statusZahteve").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });

    }
    preberiMeritveVitalnihZnakov();
}

function gen_tlak_sis(base) {
    return base + Math.floor(Math.random()*20+1);
}

function gen_tlak_dia(base) {
    return base + Math.floor(Math.random()*20+1);
}

function gen_temp(base) {
    return base + Math.floor(Math.random()*2+1);
}

function gen_O2(base) {
    return base + Math.floor(Math.random()*2+1);
}