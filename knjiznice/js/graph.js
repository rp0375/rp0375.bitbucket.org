 $(document).ready(function() {
     preberiMeritveVitalnihZnakov();
 });
 
function preberiMeritveVitalnihZnakov() {
 
    var sessionId = getSessionId();
    var ehrId = $("#preberiObstojeciEHR").val();
    console.log("GetEHR:", ehrId);

    console.log("Zahtevam podatke za " + ehrId);

    if (!ehrId || ehrId.trim().length == 0) {
    $("#statusZahteve").html("<span class='obvestilo " +
            "label label-warning fade-in'>Vnešen ehrID ni veljaven");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var party = data.party;

                var AQL =
                    "select \
                        t/data[at0001]/events[at0006]/time/value as cas, \
                    t/data[at0001]/events[at0006]/data[at0003]/items[at0004]/value/magnitude as tlak_s, \
                        t/data[at0001]/events[at0006]/data[at0003]/items[at0005]/value/magnitude as tlak_d \
                        from EHR e[e/ehr_id/value='" + ehrId + "'] \
                        contains OBSERVATION t[openEHR-EHR-OBSERVATION.blood_pressure.v1] \
                        order by t/data[at0001]/events[at0006]/time/value desc \
                        limit 31";
                $.ajax({
                    url: baseUrl + "/query?" + $.param({"aql": AQL}),
                    type: 'GET',
                    headers: {"Ehr-Session": sessionId},
                    success: function (res) {
                        if (res) {
                            $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" +
                                "Poizvedba uspela!</span>");

                            var arrlen = res.resultSet.length;
                            var casX = [];
                        var diaY = [];
                            var sisY = [];
                            for (var f = 0; f < arrlen; f++) {
                                casX[f] = f;
                                diaY[f] = res.resultSet[arrlen-f-1].tlak_d;
                                sisY[f] = res.resultSet[arrlen-f-1].tlak_s;
                            }

                            dia = {
                                x: casX,
                                y: diaY,
                                type: 'lines+markers',
                                name: 'Diastola'
                            };
                            sis = {
                                x: casX,
                                y: sisY,
                                type: 'lines+markers',
                                name: 'Sistola'
                            };

                            var data = [dia, sis];
                            var avgDia = 0;
                            var avgSis = 0;
                            
                            for(var i in data[0].y) {
                                avgDia += data[0].y[i];
                            }
                            avgDia = avgDia / data[0].y.length;
                            
                            for(var i in data[1].y) {
                                avgSis += data[1].y[i];
                            } 
                            avgSis = avgSis / data[1].y.length;
                            
                            avgDia = Math.floor(avgDia);
                            avgSis = Math.floor(avgSis);
                            
                            console.log(avgDia, avgSis);
                            document.getElementById("status").style.textAlign="center";
                            if((avgDia<=90 && avgDia>=60) && (avgSis<=140 && avgSis>=110)) {
                                document.getElementById("status").innerHTML="Zdravi ste. Kar tako naprej!";
                            }
                            else if (avgDia > 90 || avgDia < 60) {
                                document.getElementById("status").innerHTML="Diastolicni krvni tlak(tlak, ko srce počiva in nastopi tišina) je pomanjšan/povišan. Obiščite zdravnika.";
                            }
                            else if (avgSis > 140 || avgSis < 110) {
                                document.getElementById("status").innerHTML="Sistolicni krvni tlak(tlak, ko srce potisne kri po žili) je pomanjšan/povišan. Obiščite zdravnika.";
                            }
                            var layout = {
                                autosize: true,
                                height: 230,
                                margin: {
                                    l: 50,
                                    r: 0,
                                    b: 50,
                                    t: 0,
                                    pad: 4
                                },
                                yaxis: {
                                    range: [0, 200],
                                    autorange: false
                                },
                                xaxis: {
                                    range: [0, 31],
                                    autorange: false
                                }
                            };
                            $("#graph").html("");
                        Plotly.newPlot('graph', data, layout);

                        } else {
                            $("#statusZahteve").html(
                                "<span class='obvestilo label label-warning fade-in'>" +
                                "Ni podatkov!</span>");
                        }

                    },
                    error: function(err) {
                        $("#statusZahteve").html(
                            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                            JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
            },
            error: function(err) {
                $("#statusZahteve").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}