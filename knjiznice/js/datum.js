$(document).ready(function() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    
    if(dd<10) {
        dd='0'+dd;
    } 
    
    if(mm<10) {
        mm='0'+mm;
    } 
    
    today = yyyy + '-' + mm + '-' + dd;
    
    document.getElementById('dodajDatum').placeholder = today;
});
function randomTimeDate() {
    var leto = Math.floor(Math.random()*55+1960);
    var mesec = Math.floor(Math.random()*12+1);

    var danZgM;

if(mesec == 1 || mesec == 3 || mesec == 5 || mesec == 7 || mesec == 8 || mesec == 10 || mesec == 12)
        danZgM = 31;
    else if(mesec == 2)
        danZgM = 28;
    else
        danZgM = 30;

    var dan = Math.floor(Math.random()*danZgM+1);
    var ura = Math.floor(Math.random()*23);
    var minuta = Math.floor(Math.random()*60+1);
    var sekunda = Math.floor(Math.random()*60);

    return leto + "-" + mesec + "-" + dan + "T" + ura + ":" + minuta + ":" + sekunda + "Z";
}