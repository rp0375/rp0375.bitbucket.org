var bolnik_prvi_EhrId = null;
var bolnik_drugi_EhrId = null;
var bolnik_tretji_EhrId = null;
var sessionId;
    var baseUrl = 'https://rest.ehrscape.com/rest/v1';
    var queryUrl = baseUrl + '/query';
/*
* Funkcija generiranjePodatkov obnovi podatke, če so bili izbrisani
* pred generiranjem je nujno, da so bolniki kreirani in imajo svoj
* EhrId. Uporabnik je opozorjen, če tega še ni.
 */
function generiranjePodatkov() {
    if (bolnik_prvi_EhrId === null || bolnik_drugi_EhrId === null || bolnik_tretji_EhrId === null) {
        alert("Prosim najprej kreiraj bolnike!");
    } else {
    prviRecs(bolnik_prvi_EhrId);
        drugiRecs(bolnik_drugi_EhrId);
        tretjiRecs(bolnik_tretji_EhrId);
    }
}

/*
* Funkcija generirajPodatke glede na tri parametre s POST zahtevo pošlje podatke na API
* @param ehrId - identifikacijska številka
* @param podatkiVS - podatki o bolniku {datumInUra, telesnaTemp, telesnaTeza, telesnaVisina, sisTlak, diasTlak, nasicSKis, merilec, }
* @param podatkiMed - podatki o zdravniku {zdravnik, zdUstanova, navodila, casInt, kolicinaDnevno, zac, kon, smernice, zdravilo}
*
* Hvala: https://github.com/ActoreX/ois-dn4/
 */
function generirajPodatke(ehrId, podatkiVS, podatkiMed) {
var sessionId = getSessionId();
 var p = $.Deferred();
    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });

    var datumInUra = podatkiVS["datumInUra"];
    var telesnaTemperatura = podatkiVS["telesnaTemp"];
    var telesnaTeza = podatkiVS["telesnaTeza"];
    var telesnaVisina = podatkiVS["telesnaVisina"];
    var sistolicniKrvniTlak = podatkiVS["sisTlak"];
    var diastolicniKrvniTlak = podatkiVS["diasTlak"];
    var nasicenostKrviSKisikom = podatkiVS["nasicSKis"];
    var merilec = podatkiVS["merilec"];


    var podatki = {
    "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datumInUra,
        "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
        "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
        "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
        "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
        "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
        "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
    };

    var parametriZahteve = {
        "ehrId": ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: merilec
    };

    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        success: function (res) {
            console.log(res.meta.href);
        },
    error: function(err) {
            console.log(JSON.parse(err.responseText).userMessage);
        }
    });

    var zdravnik = podatkiMed["zdravnik"];
    var zdUstanova = podatkiMed["zdUstanova"];
    var narrative = podatkiMed["navodila"];
    var timingval = podatkiMed["casInt"];
    var timingdc = podatkiMed["kolicinaDnevno"];
    var startDate = podatkiMed["zac"];
    var endDate  = podatkiMed["kon"];
    var medDir = podatkiMed["smernice"];
    var medicine = podatkiMed["zdravilo"];

    podatki = {
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datumInUra,
        "ctx/id_namespace":zdUstanova,
        "ctx/participation_name":zdravnik,
        "ctx/participation_function":"requester",
        "ctx/participation_mode":"face-to-face communication",
        "medications/medication_instruction:0/order:0/medicine": medicine,
        "medications/medication_instruction:0/order:0/directions":medDir,
        "medications/medication_instruction:0/order:0/dose/quantity|unit": Math.floor(Math.random()*5+1),
    "medications/medication_instruction:0/order:0/medication_timing/timing/daily_count": timingdc,
        "medications/medication_instruction:0/order:0/medication_timing/start_date": startDate,
        "medications/medication_instruction:0/order:0/medication_timing/stop_date": endDate,

        // bugg fix : Composition validation failed (there are missing or invalid values).
        'medications/medication_instruction/order/timing|formalism': 'timing',
    'medications/medication_instruction/order/timing|value': timingval,
        'medications/medication_instruction/narrative': narrative
    };

    parametriZahteve = {
        "ehrId": ehrId,
        templateId: 'Medications',
        format: 'FLAT',
        committer: zdravnik
    };

    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        success: function (res) {
            console.log(res.meta.href);
        },
        error: function(err) {
            console.log(JSON.parse(err.responseText).userMessage);
        }
    });
    

    setTimeout(function (){
        // and call `resolve` on the deferred object, once you're done
        p.resolve();
    }, 5000);

    $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" + "Generirani!</span>");

   // return the deferred object
   return p;
}

/*
* Funkcija kreirajEhrZaBolnike na API pošlje zahtevo za ustvarjanje
+* bolnika. V spremenljivke bolnik_prvi_EhrId, bolnik_drugi_EhrId,
* bolnik_tretji_EhrId zapiše njihove nove ehr IDje
*
* Hvala: https://github.com/ActoreX/ois-dn4/
*/
+function kreirajEhrZaBolnike() {
    var r = $.Deferred();

    sessionId = getSessionId();

    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });

    // bolnik 1
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',

        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: "Rok",
                lastNames: "P",
                dateOfBirth: "1998-8-29T19:20",
                partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };

            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                        bolnik_prvi_EhrId = ehrId;
                        $("#prviBolnik").val(bolnik_prvi_EhrId);
                        console.log("Prvi bolnik dobil ID");
                        console.log(bolnik_prvi_EhrId);
                    }
                },
                error: function(err) {
                    console.log("napaka!!! ehr bolnika ni bilo mogoče ustvariti! " + JSON.parse(err.responseText).userMessage);
                }
            });
        }
    });

    // Bolnik 2
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',

        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: "Happy",
                lastNames: "Tree Friend",
                dateOfBirth: "2009-3-11T19:11",
                partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };

            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                        bolnik_drugi_EhrId = ehrId;
                        $("#drugiBolnik").val(bolnik_drugi_EhrId);
                        console.log("Drugi bolnik dobil ID");
                        console.log(bolnik_drugi_EhrId);
                    }
                },
                error: function(err) {
                    console.log("napaka!!! ehr bolnika ni bilo mogoče ustvariti! " + JSON.parse(err.responseText).userMessage);
                }
            });
        }
    });

    // Bolnik 3
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',

        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: "Kenny",
                lastNames: "The Undead",
                dateOfBirth: "1982-7-12T19:10",
            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };

            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                        bolnik_tretji_EhrId = ehrId;
                        $("#tretjiBolnik").val(bolnik_tretji_EhrId);
                        console.log("Tretji bolnik dobil ID");
                        console.log(bolnik_tretji_EhrId);
                    }
                },
                error: function(err) {
                    console.log("napaka!!! ehr bolnika ni bilo mogoče ustvariti! " + JSON.parse(err.responseText).userMessage);
                }
            });
        }
    });

    setTimeout(function (){
        // and call `resolve` on the deferred object, once you're done
        r.resolve();
    }, 3000);

    // return the deferred object
    return r;
}


function prviRecs(ehrId) {
    generirajPodatke(ehrId,
        {"datumInUra":"1996-1-29T17:19:5Z", "telesnaTemp":23.2, "telesnaTeza":80, "telesnaVisina":163, "sisTlak":121, "diasTlak":60, "nasicSKis":112, "merilec":"Mike" },
        {"zdravnik":"Dr. Simon Stopar", "zdUstanova":"Zdravstveni dom Ljubljana", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":2, "zac":"1984-4-29T17:19:5Z", "kon":"1990-12-21T19:18:53.526Z", "smernice":"Pijte veliko tekocine", "zdravilo":"Amoxil"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"1997-5-12T17:19:5Z", "telesnaTemp":38.2, "telesnaTeza":98, "telesnaVisina":174, "sisTlak":115, "diasTlak":60, "nasicSKis":123, "merilec":"Stew" },
        {"zdravnik":"Dr. Nives Avancini", "zdUstanova":"Zdravstveni dom Grosuplje", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":5, "zac":"1999-4-29T17:19:5Z", "kon":"2002-12-21T19:18:53.526Z", "smernice":"Uporaba po potrebi, vendar zmerno", "zdravilo":"Aspirin"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"1998-3-11T17:19:5Z", "telesnaTemp":37.7, "telesnaTeza":90, "telesnaVisina":177, "sisTlak":122, "diasTlak":80, "nasicSKis":142, "merilec":"Boom" },
        {"zdravnik":"Dr. Peter Zorman", "zdUstanova":"Zdravstveni dom Novo Mesto", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":4, "zac":"2001-4-29T17:19:5Z", "kon":"2003-12-21T19:18:53.526Z", "smernice":"Pred uporabo bodite tesci", "zdravilo":"Omnicef"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"2010-4-21T17:19:5Z", "telesnaTemp":36.1, "telesnaTeza":88, "telesnaVisina":181, "sisTlak":131, "diasTlak":90, "nasicSKis":133, "merilec":"Tina" },
        {"zdravnik":"Dr. Marcus Johnson", "zdUstanova":"Zdravstveni dom Celje", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":3, "zac":"2002-4-29T17:19:5Z", "kon":"2005-12-21T19:18:53.526Z", "smernice":"Pred uporabo bodite tesci", "zdravilo":"Tamiflu"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"2013-6-21T17:19:5Z", "telesnaTemp":37.4, "telesnaTeza":140, "telesnaVisina":182, "sisTlak":131, "diasTlak":90, "nasicSKis":133, "merilec":"Ashely" },
        {"zdravnik":"Dr. House", "zdUstanova":"Zdravstveni dom Celje", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":3, "zac":"2003-4-29T17:19:5Z", "kon":"2004-12-21T19:18:53.526Z", "smernice":"Uporaba po potrebi, vendar zmerno", "zdravilo":"Aspirin"}
    );
}

/*
 * Funkcija kenyRecs kliče funkcijo generirajPodatke, da zapiše podatke v osebo
 */
function drugiRecs(ehrId) {
    generirajPodatke(ehrId,
        {"datumInUra":"1998-1-29T17:19:5Z", "telesnaTemp":37.2, "telesnaTeza":42, "telesnaVisina":140, "sisTlak":115, "diasTlak":80, "nasicSKis":112, "merilec":"Mike" },
        {"zdravnik":"Dr. Simon Stopar", "zdUstanova":"Zdravstveni dom Ljubljana", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":2, "zac":"2000-4-29T17:19:5Z", "kon":"2000-12-21T19:18:53.526Z", "smernice":"Pijte veliko tekocine", "zdravilo":"Percogesic"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"1999-8-12T17:19:5Z", "telesnaTemp":38.2, "telesnaTeza":44, "telesnaVisina":148, "sisTlak":155, "diasTlak":77, "nasicSKis":123, "merilec":"Stew" },
        {"zdravnik":"Dr. Nives Avancini", "zdUstanova":"Zdravstveni dom Grosuplje", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":5, "zac":"2000-5-29T17:19:5Z", "kon":"2003-12-21T19:18:53.526Z", "smernice":"Pijte veliko tekocine", "zdravilo":"Percogesic"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"2000-3-11T17:19:5Z", "telesnaTemp":42.7, "telesnaTeza":68, "telesnaVisina":170, "sisTlak":132, "diasTlak":70, "nasicSKis":142, "merilec":"Boom" },
        {"zdravnik":"Dr. Peter Zorman", "zdUstanova":"Zdravstveni dom Novo Mesto", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":4, "zac":"2000-7-29T17:19:5Z", "kon":"2004-12-21T19:18:53.526Z", "smernice":"Pijte veliko tekocine", "zdravilo":"BeFlex"}
    );
}

/*
 * Funkcija chuckRecs kliče funkcijo generirajPodatke, da zapiše podatke v osebo
 */
function tretjiRecs(ehrId) {
    generirajPodatke(ehrId,
        {"datumInUra":"1998-1-29T17:19:5Z", "telesnaTemp":34.2, "telesnaTeza":100, "telesnaVisina":174, "sisTlak":121, "diasTlak":60, "nasicSKis":112, "merilec":"Mike" },
        {"zdravnik":"Dr. Simon Stopar", "zdUstanova":"Zdravstveni dom Ljubljana", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":2, "zac":"1984-4-29T17:19:5Z", "kon":"1990-12-21T19:18:53.526Z", "smernice":"Pijte veliko tekocine", "zdravilo":"Amoxil"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"1998-5-12T17:19:5Z", "telesnaTemp":38.2, "telesnaTeza":111, "telesnaVisina":175, "sisTlak":115, "diasTlak":60, "nasicSKis":123, "merilec":"Stew" },
        {"zdravnik":"Dr. Nives Avancini", "zdUstanova":"Zdravstveni dom Grosuplje", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":5, "zac":"1999-4-29T17:19:5Z", "kon":"2002-12-21T19:18:53.526Z", "smernice":"Uporaba po potrebi, vendar zmerno", "zdravilo":"Aspirin"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"1999-3-11T17:19:5Z", "telesnaTemp":37.7, "telesnaTeza":102, "telesnaVisina":176, "sisTlak":122, "diasTlak":80, "nasicSKis":142, "merilec":"Boom" },
        {"zdravnik":"Dr. Peter Zorman", "zdUstanova":"Zdravstveni dom Novo Mesto", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":4, "zac":"2001-4-29T17:19:5Z", "kon":"2003-12-21T19:18:53.526Z", "smernice":"Pred uporabo bodite tesci", "zdravilo":"Omnicef"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"2010-4-21T17:19:5Z", "telesnaTemp":36.1, "telesnaTeza":99, "telesnaVisina":177, "sisTlak":131, "diasTlak":90, "nasicSKis":133, "merilec":"Tina" },
        {"zdravnik":"Dr. Marcus Johnson", "zdUstanova":"Zdravstveni dom Celje", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":3, "zac":"2002-4-29T17:19:5Z", "kon":"2005-12-21T19:18:53.526Z", "smernice":"Pred uporabo bodite tesci", "zdravilo":"Tamiflu"}
    );
    generirajPodatke(ehrId,
        {"datumInUra":"2013-3-21T17:19:5Z", "telesnaTemp":40.4, "telesnaTeza":98, "telesnaVisina":178, "sisTlak":131, "diasTlak":90, "nasicSKis":133, "merilec":"Ashely" },
        {"zdravnik":"Dr. House", "zdUstanova":"Zdravstveni dom Celje", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":3, "zac":"2003-4-29T17:19:5Z", "kon":"2004-12-21T19:18:53.526Z", "smernice":"Uporaba po potrebi, vendar zmerno", "zdravilo":"Aspirin"}
    );

    generirajPodatke(ehrId,
        {"datumInUra":"2013-6-21T17:19:5Z", "telesnaTemp":37.4, "telesnaTeza":88, "telesnaVisina":179, "sisTlak":131, "diasTlak":90, "nasicSKis":133, "merilec":"Ashely" },
        {"zdravnik":"Dr. Who", "zdUstanova":"Zdravstveni dom Celje", "navodila":"Jemlji na 2 krat na dan", "casInt":4, "kolicinaDnevno":3, "zac":"2003-4-29T17:19:5Z", "kon":"2004-12-21T19:18:53.526Z", "smernice":"Uporaba po potrebi, vendar zmerno", "zdravilo":"Aspirin"}
    );
}