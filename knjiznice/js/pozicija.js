var mapProp;
var map;
var marker;
function myMap() {
    mapProp= {
        center:new google.maps.LatLng(46.0565, 14.5081),
        zoom:9,
    };
    marker = new google.maps.Marker(
        {
            position: mapProp.center,
            animation: google.maps.Animation.BOUNCE
        }
    );
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((pos) => {
            mapProp = {
                center: new google.maps.LatLng(
                    pos.coords.latitude,
                    pos.coords.longitude
                ),
                zoom: 15
            }
            marker = new google.maps.Marker(
                {
                    position: mapProp.center,
                    animation: google.maps.Animation.BOUNCE
                });
            map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            marker.setMap(map);
        }, () => {
            map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            marker.setMap(map);
        });
    }
    else {
        map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
        marker.setMap(map);
    }
    
    
}